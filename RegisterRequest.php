<?php
/**
 * Created by IntelliJ IDEA.
 * User: ivanr
 * Date: 31/10/2017
 * Time: 13:35
 */

class RegisterRequest
{
    public $name;
    public $email;
    public $password;
    public $passwordConfirmation;
}