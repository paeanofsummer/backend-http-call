<?php
/**
 * Created by IntelliJ IDEA.
 * User: ivanr
 * Date: 31/10/2017
 * Time: 14:29
 */

class RegisterResponse
{

    public $status;
    public $message;
    public $token;
    public $expiresIn;
    public $accessToken;
    public $refreshToken;

}