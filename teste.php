<?php
/**
 * Created by IntelliJ IDEA.
 * User: ivanr
 * Date: 31/10/2017
 * Time: 13:13
 */

include('RegisterRequest.php');
include('RegisterResponse.php');

$url = "http://localhost:8080/api/register";

//echo $_POST['username'];
//echo $_POST['email'];
//echo $_POST['password'];
//echo $_POST['passwordConfirmation'];


$registerRequest = new RegisterRequest();
$registerRequest->name = $_POST['username'];
$registerRequest->email = $_POST['email'];
$registerRequest->password = $_POST['password'];
$registerRequest->passwordConfirmation = $_POST['passwordConfirmation'];
$jsontext = json_encode((array)$registerRequest);


$client = curl_init($url);
curl_setopt($client, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($client, CURLOPT_POSTFIELDS, $jsontext);
curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
curl_setopt($client, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsontext))
);
$json = curl_exec($client);

$data = json_decode($json, true);

$class = new RegisterResponse();
foreach ($data as $key => $value)
    $class->{$key} = $value;

$httpcode = curl_getinfo($client, CURLINFO_HTTP_CODE);
curl_close($client);

//if ($httpcode == 201) {  //todo retorno de sucess e erro
//    echo "sucesso";
//} else {
////    header('HTTP/1.1 500 Internal Server Booboo');
//    header('Content-Type: application/json; charset=UTF-8');
//    die(json_encode(array('message' => $class->message, 'status' => $class->status)));
//}